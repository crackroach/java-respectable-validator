package utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * An utility tool for Numbers
 *
 * @see java.lang.Number
 */
public class NumberUtils {

    protected Number number;

    /**
     * Create a NumberUtils instance to facilitate multiple test on the same Number instance
     * @param number to be tested or used
     */
    public NumberUtils(Number number) {
        this.number = number;
    }

    public Number getNumber() {

        return number;
    }

    public void setNumber(Number number) {

        this.number = number;
    }

    public boolean isByte() {
        return isByte(number);
    }

    public boolean isShort() {
        return isShort(number);
    }

    public boolean isInteger() {
        return isInteger(number);
    }

    public boolean isLong() {
        return isLong(number);
    }

    public boolean isFloat() {
        return isFloat(number);
    }

    public boolean isBigInteger() {
        return isBigInteger(number);
    }

    public boolean isAtomicInteger() {
        return isAtomicInteger(number);
    }

    public boolean isAtomicLong() {
        return isAtomicLong(number);
    }

    public boolean isDouble() {
        return isDouble(number);
    }

    public boolean isBigDecimal() {
        return isBigDecimal(number);
    }

    /**
     * @return if number is a Float, a Double or a BigDecimal
     */
    public boolean isDecimal() {
        return isDecimal(number);
    }

    public static boolean isByte(Number n) {
        return n instanceof Byte;
    }

    public static boolean isShort(Number n) {
        return n instanceof Short;
    }

    public static boolean isInteger(Number n) {
        return n instanceof Integer;
    }

    public static boolean isLong(Number n) {
        return n instanceof Long;
    }

    public static boolean isFloat(Number n) {
        return n instanceof Float;
    }

    public static boolean isBigInteger(Number n) {
        return n instanceof BigInteger;
    }

    public static boolean isAtomicInteger(Number n) {
        return n instanceof AtomicInteger;
    }

    public static boolean isAtomicLong(Number n) {
        return n instanceof AtomicLong;
    }

    public static boolean isDouble(Number n) {
        return n instanceof Double;
    }

    public static boolean isBigDecimal(Number n) {
        return n instanceof BigDecimal;
    }

    /**
     * @return if number is a Float, a Double or a BigDecimal
     */
    public static boolean isDecimal(Number number) {
        return isDouble(number) || isFloat(number) || isBigDecimal(number);
    }
}
