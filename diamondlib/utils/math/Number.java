package utils.number;

import java.math.BigDecimal;

public class Number extends java.lang.Number {

    Double number;

    public Number(Byte number) {
        this.number = number.doubleValue();
    }

    public Number(Short number) {
        this.number = number.doubleValue();
    }

    public Number(Integer number) {
        this.number = number.doubleValue();
    }

    public Number(Long number) {
        this.number = number.doubleValue();
    }

    public Number(Float number) {
        this.number = number.doubleValue();
    }

    public Number(Double number) {
        this.number = number;
    }

    @Override
    public int intValue() {

        return number.intValue();
    }

    @Override
    public long longValue() {

        return number.longValue();
    }

    @Override
    public float floatValue() {

        return number.floatValue();
    }

    @Override
    public double doubleValue() {

        return number.doubleValue();
    }
}
