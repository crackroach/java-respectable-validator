package validation2.validators;

import validation2.AbstractValidator;

import java.util.Arrays;
import java.util.List;

/**
 * An abstract class that is used to validate multiple sub subvalidators.
 * <p>
 * Every subvalidators that have more than one sub subvalidator should extends this class.
 * <p>
 * @see validation2.AbstractValidator
 */
public abstract class AbstractCompositeSubValidator extends AbstractValidator {

    /**
     * A list of subvalidator to be evaluated all together
     */
    protected List<AbstractValidator> subvalidators;

    /**
     * Define a constructor of AbstractCompositeSubValidator with next subvalidator.
     * @param subvalidators all the subvalidators needed to pass the criteria passed as a varargs.
     */
    public AbstractCompositeSubValidator(AbstractValidator... subvalidators) {
        this(null, Arrays.asList(subvalidators));
    }

    /**
     * Define a constructor of AbstractCompositeSubValidator.
     * @param subvalidators all the subvalidators needed to pass the criteria.
     */
    public AbstractCompositeSubValidator(List<AbstractValidator> subvalidators) {

       this(null, subvalidators);
    }

    /**
     * Define a constructor of AbstractCompositeSubValidator with next subvalidator.
     * @param next the next subvalidator to be executed.
     * @param subvalidators all the subvalidators needed to pass the criteria passed as a varargs.
     */
    public AbstractCompositeSubValidator(AbstractValidator next, AbstractValidator... subvalidators) {
        this(next, Arrays.asList(subvalidators));
    }

    /**
     * Define a constructor of AbstractCompositeSubValidator.
     * @param next subvalidator to be executed.
     * @param subvalidators all the subvalidators needed to pass the criteria.
     */
    public AbstractCompositeSubValidator(AbstractValidator next, List<AbstractValidator> subvalidators) {

        super(next);
        if(subvalidators.size() == 0)
            throw new IllegalArgumentException("subvalidators cannot be null nor empty");
        if(subvalidators.size() < 2) {
            throw new IllegalArgumentException("subvalidators must at least contains 2 items");
        }
        this.subvalidators = subvalidators;
    }

    public List<AbstractValidator> getSubvalidators() {

        return subvalidators;
    }
}
