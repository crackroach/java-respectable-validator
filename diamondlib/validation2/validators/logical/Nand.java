package validation2.validators.logical;

import validation2.AbstractValidator;
import validation2.validators.AbstractCompositeSubValidator;

import java.util.List;

/**
 * A NAND GATE logical operator packed inside a validator for convenience
 *
 * <table style="border-collapse:collapse;border-spacing:0"><caption></caption><tr><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;
 * border-width:1px;overflow:hidden;word-break:normal;border-color:black" colspan="2">INPUT</th><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">OUTPUT</th></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">A</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">B</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">A NAND B</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td></tr></table>
 */
public class Nand extends AbstractCompositeSubValidator {

    /**
     * Create a NAND gate subvalidator.
     * @param validators as varargs to be tested
     */
    public Nand(AbstractValidator... validators) {

        super(validators);
    }

    /**
     * Create a NAND gate subvalidator.
     * @param validators to be tested
     */
    public Nand(List<AbstractValidator> validators) {

        super(validators);
    }

    /**
     * Create a NAND gate subvalidator.
     * @param next subvalidator to be executed
     * @param validators as varargs to be tested
     */
    public Nand(AbstractValidator next, AbstractValidator... validators) {

        super(next, validators);
    }

    /**
     * Create a NAND gate subvalidator.
     * @param next subvalidator to be executed
     * @param validators to be tested
     */
    public Nand(AbstractValidator next, List<AbstractValidator> validators) {

        super(next, validators);
    }

    /**
     * Internally uses an AND subvalidator and invert it with a NOT subvalidator.
     * @return the inverted value of AND subvalidator.
     *
     * @see validation2.validators.logical.And
     * @see validation2.validators.logical.Not
     */
    @Override
    public boolean meetCriteria() {

        AbstractValidator not = new Not(new And(next, subvalidators));

        return not.meetCriteria();
    }
}
