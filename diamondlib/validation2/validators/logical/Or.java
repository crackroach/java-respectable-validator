package validation2.validators.logical;

import validation2.AbstractValidator;
import validation2.validators.AbstractCompositeSubValidator;

import java.util.List;

/**
 * Logical OR GATE in a validator format. Accept at least 2 sub validators.
 *
 * <table style="border-collapse:collapse;border-spacing:0"><caption></caption><tr><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;
 * border-style:solid;
 * border-width:1px;
 * overflow:hidden;word-break:normal;border-color:black" colspan="2">INPUT</th><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;
 * border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">OUTPUT</th></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">A</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">B</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">A OR B</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td></tr></table>
 */
public class Or extends AbstractCompositeSubValidator {

    /**
     * Create a new OR subvalidator.
     * @param validators to be validated as varargs.
     */
    public Or(AbstractValidator... validators) {

        super(validators);
    }

    /**
     * Create a new OR subvalidator.
     * @param validators to be validated.
     */
    public Or(List<AbstractValidator> validators) {

        super(validators);
    }

    /**
     * Create a new OR subvalidator.
     * @param next subvalidator to be executed.
     * @param validators to be validated as varargs.
     */
    public Or(AbstractValidator next, AbstractValidator... validators) {

        super(next, validators);
    }

    /**
     * Create a new OR subvalidator.
     * @param next subvalidator to be executed.
     * @param validators to be validated.
     */
    public Or(AbstractValidator next, List<AbstractValidator> validators) {

        super(next, validators);
    }

    /**
     * Compare all subvalidator and if at leaast one matches its criteria, the current subvalidator passes.
     * @return if at least one of the subvalidator meets the criteria.
     */
    @Override
    public boolean meetCriteria() {

        boolean cleared = false;

        for(AbstractValidator av : subvalidators) {
            cleared = Boolean.logicalOr(cleared, av.meetCriteria());
            if (cleared) break;
        }

        return cleared;
    }
}
