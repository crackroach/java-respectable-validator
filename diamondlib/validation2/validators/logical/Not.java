package validation2.validators.logical;

import validation2.AbstractValidator;
import validation2.validators.AbstractSimpleSubValidator;

/**
 * A NOT gate inside a validator.
 * Inverta subvalidator.
 *
 * <table style="border-collapse:collapse;border-spacing:0"><caption></caption><tr><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;
 * border-width:1px;overflow:hidden;word-break:normal;border-color:black">INPUT</th><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black">OUTPUT</th></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black">A</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black">NOT A</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black">1</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black">0</td></tr></table>
 */
public class Not extends AbstractSimpleSubValidator {

    /**
     * Create an instance of Not subvalidator.
     * @param validator to invert.
     */
    public Not(AbstractValidator validator) {

        super(validator);
    }

    /**
     * Create an instance of Not subvalidator.
     * @param next the next subvalidator to execute.
     * @param validator to invert.
     */
    public Not(AbstractValidator next, AbstractValidator validator) {

        super(next, validator);
    }

    /**
     * Invert the value of a sub subvalidator.
     * @return the inverted value of the sub subvalidator.
     */
    @Override
    public boolean meetCriteria() {

        return !(subvalidator.validate().getNumberOfError() == 0);
    }
}
