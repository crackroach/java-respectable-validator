package validation2.validators.logical;

import validation2.AbstractValidator;
import validation2.validators.AbstractCompositeSubValidator;

import java.util.List;

/**
 * A XNOR gate packed inside a validator.
 * <p>Uses a XOR gate and a NOT gate internally</p>
 * @see validation2.validators.logical.Xor
 * @see validation2.validators.logical.Not
 *
 * <table style="border-collapse:collapse;border-spacing:0"><caption></caption><tr><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;
 * border-width:1px;overflow:hidden;word-break:normal;border-color:black" colspan="2">INPUT</th><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">OUTPUT</th></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">A</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">B</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">A XNOR B</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td></tr></table>
 */
public class XNor extends AbstractCompositeSubValidator {


    /**
     * Create a XNOR subvalidator
     * @param validators all the subvalidators needed to pass the criteria passed as a varargs.
     */
    public XNor(AbstractValidator... validators) {

        super(validators);
    }

    /**
     * Create a XNOR subvalidator.
     * @param validators all the subvalidators needed to pass the criteria passed.
     */
    public XNor(List<AbstractValidator> validators) {

        super(validators);
    }

    /**
     * Create a XNOR subvalidator with next subvalidator.
     * @param next subvalidator to be executed.
     * @param validators all the subvalidators needed to pass the criteria passed as a varargs.
     */
    public XNor(AbstractValidator next, AbstractValidator... validators) {

        super(next, validators);
    }

    /**
     * Create a XNOR subvalidator with next subvalidator.
     * @param next subvalidator to be executed
     * @param validators all the subvalidators needed to pass the criteria passed as a varargs.
     */
    public XNor(AbstractValidator next, List<AbstractValidator> validators) {

        super(next, validators);
    }

    /**
     * Internally uses a XOR subvalidator inverted by a NOT subvalidator.
     * @return the inverted value of a XOR subvalidator
     *
     * @see validation2.validators.logical.Xor
     * @see validation2.validators.logical.Not
     */
    @Override
    public boolean meetCriteria() {

        Not not = new Not(new Xor(next, subvalidators));
        return not.meetCriteria();
    }

    @Override
    protected String getErrorMessage() {

        return "Either all entries failed to meet their criteria or all of them passed.";
    }

}
