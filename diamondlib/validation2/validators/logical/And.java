package validation2.validators.logical;

import validation2.AbstractValidator;
import validation2.validators.AbstractCompositeSubValidator;

import java.util.List;

/**
 * Logical AND gate validator
 *
 * <table style="border-collapse:collapse;border-spacing:0"><caption></caption><tr><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;
 * border-width:1px;overflow:hidden;word-break:normal;border-color:black" colspan="2">INPUT</th><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">OUTPUT</th></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">A</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">B</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">A AND B</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td></tr></table>
 */
public class And extends AbstractCompositeSubValidator {

    /**
     * Create an AND gate subvalidator.
     * <p></p>
     * Last subvalidator.
     *
     * @param validators to be used
     */
    public And(AbstractValidator... validators) {

        super(validators);
    }

    /**
     * Create an AND gate subvalidator.
     * <p></p>
     * Last subvalidator.
     *
     * @param validators to be used.
     */
    public And(List<AbstractValidator> validators) {

        super(validators);
    }

    /**
     * Create an AND gate subvalidator.
     *
     * @param next subvalidator to be executed.
     * @param validators to be used.
     */
    public And(AbstractValidator next, AbstractValidator... validators) {

        super(next, validators);
    }

    /**
     * Create an AND gate subvalidator.
     *
     * @param next subvalidator to be executed
     * @param validators to be used.
     */
    public And(AbstractValidator next, List<AbstractValidator> validators) {

        super(next, validators);
    }

    /**
     * Test all sub subvalidators for an AND gate.
     * @return if all the sub subvalidators passed with result TRUE
     */
    @Override
    public boolean meetCriteria() {

        for(AbstractValidator av : subvalidators) {
            if(!av.meetCriteria())
                return false;
        }

        return true;
    }
}
