package validation2.validators.logical;

import validation2.AbstractValidator;
import validation2.validators.AbstractCompositeSubValidator;

import java.util.List;

/**
 * A NOR gate packed inside a validator.
 *
 * <table style="border-collapse:collapse;border-spacing:0"><tr><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black" colspan="2">INPUT</th><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">OUTPUT</th></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">A</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">B</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">A NOR B</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td></tr><tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">1</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;vertical-align:top">0</td></tr></table>
 */
public class Nor extends AbstractCompositeSubValidator {

    /**
     * Create a new NOR subvalidator.
     * @param validators  as varargs to be tested.
     */
    public Nor(AbstractValidator... validators) {

        super(validators);
    }

    /**
     * Create a new NOR subvalidator.
     * @param validators to be tested.
     */
    public Nor(List<AbstractValidator> validators) {

        super(validators);
    }

    /**
     * Create a new NOR subvalidator.
     * @param next the next subvalidator to be executed.
     * @param validators  as varargs to be tested.
     */
    public Nor(AbstractValidator next, AbstractValidator... validators) {

        super(next, validators);
    }

    /**
     * Create a new NOR subvalidator.
     * @param next subvalidator to be executed
     * @param validators to be tested.
     */
    public Nor(AbstractValidator next, List<AbstractValidator> validators) {

        super(next, validators);
    }

    /**
     * Internally uses an OR subvalidator and the NOT subvalidator.
     * @return the inverted value of an OR subvalidator.
     *
     * @see validation2.validators.logical.Or
     * @see validation2.validators.logical.Not
     */
    @Override
    public boolean meetCriteria() {

        AbstractValidator not = new Not(new Or(next, subvalidators));
        return not.meetCriteria();
    }
}
