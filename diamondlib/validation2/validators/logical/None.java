package validation2.validators.logical;

import validation2.AbstractValidator;
import validation2.validators.AbstractCompositeSubValidator;

import java.util.List;

/**
 * Test that no validator passes their tests
 */
public class None extends AbstractCompositeSubValidator {

    public None(AbstractValidator... subvalidators) {

        super(subvalidators);
    }

    public None(List<AbstractValidator> subvalidators) {

        super(subvalidators);
    }

    public None(AbstractValidator next, AbstractValidator... subvalidators) {

        super(next, subvalidators);
    }

    public None(AbstractValidator next, List<AbstractValidator> subvalidators) {

        super(next, subvalidators);
    }

    @Override
    public boolean meetCriteria() {

        AbstractValidator and = new And(next, subvalidators);
        return and.validate().getNumberOfValid() == 0;
    }
}
