package validation2.validators.file;

import validation2.AbstractValidator;

import java.io.File;

/**
 * Test the file for a given extension.
 * Note that if the file is named <i>example.tar.gz</i>, only <i>gz</i> will be tested
 */
public class ExtensionIs extends AbstractFileValidator {

    /**
     * The file extension to test
     */
    protected String extension;

    /**
     * Test the file for a given extension.
     * Note that if the file is named <i>example.tar.gz</i>, only <i>gz</i> will be tested
     * @param file to test
     * @param extension that is supposed to be there
     */
    public ExtensionIs(File file, String extension) {

        this(null, file, extension);
    }

    /**
     * Test the file for a given extension.
     * Note that if the file is named <i>example.tar.gz</i>, only <i>gz</i> will be tested
     * @param next validator to be executed
     * @param file to test
     * @param extension that is supposed to be there
     */
    public ExtensionIs(AbstractValidator next, File file, String extension) {

        super(next, file);
        if(extension.length() <= 0)
            throw new IllegalArgumentException("extension cannot be empty. To test an empty extension use the 'HasExtension' validator");
        this.extension = extension;
    }

    /**
     * Test the file for a given extension.
     * Note that if the file is named <i>example.tar.gz</i>, only <i>gz</i> will be tested
     * @param file path to create a file from. File to test
     * @param extension that is supposed to be there
     */
    public ExtensionIs(String file, String extension) {

        this(null, file, extension);
    }

    /**
     * Test the file for a given extension.
     * Note that if the file is named <i>example.tar.gz</i>, only <i>gz</i> will be tested
     * @param file path to test
     * @param extension that is supposed to be there
     */
    public ExtensionIs(AbstractValidator next, String file, String extension) {

        this(next, new File(file), extension);
    }

    @Override
    public boolean meetCriteria() {

        String path = file.getPath();

        int index = path.lastIndexOf(".");
        if(index > 0) {
            String ext = path.substring(index + 1);
            return ext.equalsIgnoreCase(extension);
        }
        return false;
    }
}
