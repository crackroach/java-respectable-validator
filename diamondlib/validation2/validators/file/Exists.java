package validation2.validators.file;

import validation2.AbstractValidator;

import java.io.File;

/**
 * Test if a file exists
 */
public class Exists extends AbstractFileValidator {


    public Exists(File file) {

        super(file);
    }

    public Exists(AbstractValidator next, File file) {

        super(next, file);
    }

    public Exists(String file) {

        super(file);
    }

    public Exists(AbstractValidator next, String file) {

        super(next, file);
    }

    /**
     * Test if a file exists
     * @return true if the file exists
     */
    @Override
    public boolean meetCriteria() {

        return file.exists();
    }
}
