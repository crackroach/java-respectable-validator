package validation2.validators.file;

import validation2.AbstractValidator;

import java.io.File;

/**
 * Abstract File validator for file validation2
 */
public abstract class AbstractFileValidator extends AbstractValidator {

    /**
     * The file that will be evaluated
     */
    protected File file;

    /**
     * Define a constructor with a single file to be tested
     * @param file to be tested
     */
    public AbstractFileValidator(File file) {

        this.file = file;
    }

    /**
     * Define a constructor with a single file to be tested
     * @param next validator to be executed
     * @param file tha will be evaluated
     */
    public AbstractFileValidator(AbstractValidator next, File file) {

        super(next);
        this.file = file;
    }

    /**
     * Define a constructor with a single file to be tested.
     * @param file the path of the wanted file
     */
    public AbstractFileValidator(String file) {
        this(new File(file));
    }

    /**
     * Define a constructor with a single fiel to be tested
     * @param next validator to be executed
     * @param file path of the wanted file
     */
    public AbstractFileValidator(AbstractValidator next, String file) {
        this(next, new File(file));
    }
}
