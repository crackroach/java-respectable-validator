package validation2.validators.file;

import validation2.AbstractValidator;

import java.io.File;

/**
 * Test if the file has an extension
 */
public class HasExtension extends AbstractFileValidator {

    public HasExtension(File file) {

        super(file);
    }

    public HasExtension(AbstractValidator next, File file) {

        super(next, file);
    }

    public HasExtension(String file) {

        super(file);
    }

    public HasExtension(AbstractValidator next, String file) {

        super(next, file);
    }

    /**
     * Test if the given file has an extension
     * @return if the file has no extension
     */
    @Override
    public boolean meetCriteria() {

        return file.getPath().lastIndexOf(".") <= 0;
    }
}
