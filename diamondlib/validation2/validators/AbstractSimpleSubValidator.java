package validation2.validators;

import validation2.AbstractValidator;

/**
 * An abstract class that is used to validate multiple sub subvalidators.
 * <p>
 * Every subvalidators that have only one sub subvalidator should extends this class.
 * <p>
 * @see validation2.AbstractValidator
 */
public abstract class AbstractSimpleSubValidator extends AbstractValidator {

    /**
     * A single sub validator to be evaluated
     */
    protected AbstractValidator subvalidator;

    /**
     * Define a constructor of AbstractSimpleSubValidator.
     * @param subvalidator to have operation on.
     */
    public AbstractSimpleSubValidator(AbstractValidator subvalidator) {

        this(null, subvalidator);
    }

    /**
     * Define a constructor of AbstractSimpleSubValidator.
     * @param next subvalidator to be executed.
     * @param subvalidator the sub subvalidator.
     */
    public AbstractSimpleSubValidator(AbstractValidator next, AbstractValidator subvalidator) {

        super(next);
        this.subvalidator = subvalidator;
    }

    public AbstractValidator getSubvalidator() {

        return subvalidator;
    }
}
