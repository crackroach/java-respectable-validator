package validation2.validators.numeric;

import com.sun.istack.internal.NotNull;
import utils.NumberUtils;
import validation2.AbstractValidator;

/**
 * Base Validator for numeric validation2
 */
public abstract class AbstractNumericValidator extends AbstractValidator {

    /**
     * The number item to be evaluated
     */
    protected Number number;

    /**
     * Define an AbstractNumericValidator.
     * @param number to be evaluated
     */
    public AbstractNumericValidator(@NotNull Number number) {

        this(null, number);
    }

    /**
     * Define a AbstractNumericValidator
     * @param next validator to be executed
     * @param number to be evaluated
     */
    public AbstractNumericValidator(AbstractValidator next, @NotNull Number number) {

        super(next);
        this.number = number;
    }

    public Double getNumber() {

        return number.doubleValue();
    }

    /**
     * Calculation for decimal typed Number
     * This includes Double, Float.
     * Does not support BigDecimal
     * @return the calculation for floating point number
     *
     * @see java.lang.Double
     * @see java.lang.Float
     */
    protected abstract boolean asDecimal(double value);

    /**
     * Calculation for integer typed Number
     * This includes Byte, Short, Integer, Long.
     * Does not support BigInteger, AtomicInteger nor AtomicLong
     * @return the calculation for integers
     *
     * @see java.lang.Byte
     * @see java.lang.Short
     * @see java.lang.Integer
     * @see java.lang.Long
     */
    protected abstract boolean asInteger(long value);

    /**
     * @return if the criteria has been met
     */
    @Override
    public boolean meetCriteria() {

        if(NumberUtils.isDecimal(number))
            return asDecimal(number.doubleValue());
        else
            return asInteger(number.longValue());
    }
}
