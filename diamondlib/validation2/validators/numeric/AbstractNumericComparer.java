package validation2.validators.numeric;

import validation2.AbstractValidator;

/**
 * Define a pattern between two number
 */
public abstract class AbstractNumericComparer extends AbstractNumericValidator {

    /**
     * The number to compare against
     */
    protected Number comparer;

    /**
     * Define a constructor for a pattern between two numbers
     * @param number to be tested
     * @param comparer to compare
     */
    public AbstractNumericComparer(Number number, Number comparer) {

        this(null, number, comparer);
    }

    /**
     *
     * @param next validator to be executed
     * @param number to be tested
     * @param comparer to compare
     */
    public AbstractNumericComparer(AbstractValidator next, Number number, Number comparer) {

        super(next, number);
        this.comparer = comparer;
    }
}
