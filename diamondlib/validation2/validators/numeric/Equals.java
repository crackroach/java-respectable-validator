package validation2.validators.numeric;

import validation2.AbstractValidator;

/**
 * Test if a number is equlas to a pattern.
 *
 */
public class Equals extends AbstractNumericComparer {
    /**
     * Create a validator to compare if a number is equals to another
     * @param number to test
     * @param comparer to test against
     */
    public Equals(Number number, Number comparer) {

        this(null, number, comparer);
    }

    /**
     * Create a validator to compare if a number is equals to another
     * @param next validator to be executed
     * @param number to test
     * @param comparer to test against
     */
    public Equals(AbstractValidator next, Number number, Number comparer) {

        super(next, number, comparer);
    }

    /**
     * Test the value as a decimal
     * @param value to be tested. Is number in double
     * @return is the test passes
     */
    @Override
    protected boolean asDecimal(double value) {

        return value == comparer.doubleValue();
    }

    /**
     * Uses asDecimal() internally.
     * @param value to be tested
     * @return if the value is greater than the pattern
     */
    @Override
    protected boolean asInteger(long value) {

        return false;
    }
}
