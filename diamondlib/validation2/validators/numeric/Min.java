package validation2.validators.numeric;

import validation2.AbstractValidator;

import java.math.BigDecimal;

/**
 * Test if a number is bigger that an other value.
 * This is a strict bigger <b>&gt;</b>
 */
public class Min extends AbstractNumericComparer {

    /**
     * Compare if the number is bigger than the pattern
     * @param number to compare
     * @param comparer to compare against
     */
    public Min(Number number, Number comparer) {

        super(number, comparer);
    }

    /**
     * Compare if the number is bigger than the pattern
     * @param next validator to execute
     * @param number to compare
     * @param comparer to compare against
     */
    public Min(AbstractValidator next, Number number, Number comparer) {

        super(next, number, comparer);
    }

    /**
     * Test the number as a decimal against the pattern
     * @param value as decimal of number
     * @return if number > pattern
     */
    @Override
    protected boolean asDecimal(double value) {

        return value > comparer.doubleValue();
    }

    /**
     * Test the number as a Long against the pattern
     * Uses asDecimal() internally
     * @param value as a long of number
     * @return if number > pattern
     */
    @Override
    protected boolean asInteger(long value) {

        return asDecimal(new Double(value));
    }
}
