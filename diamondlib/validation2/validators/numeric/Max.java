package validation2.validators.numeric;

import validation2.AbstractValidator;

/**
 * Test a value to see if it is lower than the comaprer
 * Strict lower testing <b>&lt;</b>
 */
public class Max extends AbstractNumericComparer {


    /**
     * Construct a validator to test if a number is strictly lower than another
     * Last validator
     * @param number
     * @param comparer
     */
    public Max(Number number, Number comparer) {

        super(number, comparer);
    }

    /**
     * Construct a validator to test if a number is strictly lower than another
     * @param next validator to be executed
     * @param number
     * @param comparer
     */
    public Max(AbstractValidator next, Number number, Number comparer) {

        super(next, number, comparer);
    }

    /**
     * Test the value as a decimal against the pattern as a decimal as well
     * @param value is the number casted to double
     * @return if the number is strictly lower than the pattern
     */
    @Override
    protected boolean asDecimal(double value) {

        return false;
    }

    /**
     * Uses asDecimal() internally
     * @param value to be test as a long
     * @return if the number is strictly lower than the comaprer
     */
    @Override
    protected boolean asInteger(long value) {

        return false;
    }
}
