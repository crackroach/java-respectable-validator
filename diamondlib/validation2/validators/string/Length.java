package validation2.validators.string;

import validation2.AbstractValidator;

/**
 * Check if a given String is strictly between the min and maximum length.
 */
public class Length extends AbstractStringValidator {

    /**
     * Minimum required length of a String
     */
    protected int min;
    /**
     * Maximum length of a string
     */
    protected int max;

    /**
     * Create a vlidator that test for a string length range
     * @param item is a string to test
     * @param min is the minimum size of the string. If 0 or lower is given, this parameter is ignored(i.e. no minimum size).
     * @param max is the maximum size of the string. If 0 or lower is given, this paramter is ignored(i.e. no maximum size).
     */
    public Length(String item, int min, int max) {

        super(item);
        this.min = min;
        this.max = max;
    }

    /**
     * Create a vlidator that test for a string length range
     * @param next validator
     * @param item is a string to test
     * @param min is the minimum size of the string. If 0 or lower is given, this parameter is ignored(i.e. no minimum size).
     * @param max is the maximum size of the string. If 0 or lower is given, this paramter is ignored(i.e. no maximum size).
     */
    public Length(AbstractValidator next, String item, int min, int max) {

        super(next, item);
        this.min = min;
        this.max = max;
    }

    /**
     * Test if the string length is between a given minimum and a given maximum
     * @return if the string matches the required length
     */
    @Override
    public boolean meetCriteria() {

        final boolean value;

        if(min == -1) {
            value = item.length() < max;
        }
        else if(max == -1) {
            value = item.length() > min;
        }
        else {
            value = ((item.length() > min) && (item.length() < max));
        }

        return value;
    }

    @Override
    protected String getErrorMessage() {

        final String message;

        if(min == -1)
            message = String.format("The given string have a bigger length than %s.", max);
        else if(max == -1)
            message = String.format("The given string have a smaller length than %s.", min);
        else
            message = String.format("The given string does not have a length between %s and %s.", min, max);

        return message;
    }
}
