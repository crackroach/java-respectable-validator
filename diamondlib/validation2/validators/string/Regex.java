package validation2.validators.string;

import validation2.AbstractValidator;

import java.util.regex.Pattern;

/**
 * Check if a string matches a given pattern
 */
public class Regex extends AbstractStringValidator {

    /**
     * The pattern to check
     */
    protected Pattern pattern;

    public Regex(String item, String pattern) {

        this(null, item, pattern);
    }

    public Regex(AbstractValidator next, String item, String pattern) {

        super(next, item);
        this.pattern = Pattern.compile(pattern);
    }

    @Override
    public boolean meetCriteria() {

        return pattern.matcher(item).matches();
    }

    @Override
    protected String getErrorMessage() {

        return String.format("The String doesn't match the regex pattern.");
    }
}
