package validation2.validators.string;

import validation2.AbstractValidator;

import java.util.ArrayList;

/**
 * Test if a string constains a substring
 * TODO add a case-ignore
 */
public class Contains extends AbstractStringValidator {

    /**
     * The pattern to be tested against
     */
    protected String pattern;

    /**
     * Cosntruct a validator that compares a String (item) against a pattern.
     * @param item to be evaluted.
     * @param pattern that need to be matched.
     */
    public Contains(String item, String pattern) {

        this(null, item, pattern);
    }

    /**
     * Construct a validator that compares a String item against a pattern.
     * @param next validator to be executed.
     * @param item to be evaluated.
     * @param pattern to be matched.
     */
    public Contains(AbstractValidator next, String item, String pattern) {

        super(next, item);
        this.pattern = pattern;
    }

    /**
     * Evaluate the String against a pattern
     * @return if the string contains the pattern
     */
    @Override
    public boolean meetCriteria() {

        return this.getItem().contains(pattern);
    }

    @Override
    protected String getErrorMessage() {

        return String.format("%s does not contains %s.", item, pattern);
    }


}
