package validation2.validators.string;

import validation2.AbstractValidator;

import java.util.regex.Pattern;

/**
 * Test a String to have no symbols or digits in it
 */
public class Alpha extends AbstractStringValidator {

    /**
     * A regex pattern that verify that the string only contains alphabetic character
     * i.e. [a-zA-Z]
     */
    protected static final Pattern PATTERN = Pattern.compile("[a-zA-Z]+");

    public Alpha(String item) {

        super(item);
    }

    public Alpha(AbstractValidator next, String item) {

        super(next, item);
    }

    @Override
    public boolean meetCriteria() {

        return PATTERN.matcher(item).matches();
    }

    @Override
    protected String getErrorMessage() {

        return "The String contains digits or symbol";
    }
}
