package validation2.validators.string;

import validation2.AbstractValidator;

/**
 * Test if the given string equals a certain pattern
 */
public class Equals extends AbstractStringValidator {

    /**
     * The string to compare against.
     */
    protected String  pattern;
    /**
     * Does the case matter. Defaulted at false.
     */
    protected boolean caseIgnore;

    /**
     * Create a validator that test the given string against a pattern.
     * Case sensitive. To have a case non-sensitive use the constructor Equals(String item, String pattern, boolean caseIgnore).
     * @param item to test
     * @param pattern to test against.
     */
    public Equals(String item, String pattern) {

        super(item);
        this.pattern = pattern;
        caseIgnore = false;
    }

    /**
     * Create a validator that test the given string against a pattern.
     * Case sensitive. To have a case non-sensitive use the constructor Equals(AbstractValidator next, String item, String pattern, boolean caseIgnore).
     * @param next validator to be executed
     * @param item to test
     * @param pattern to test against.
     */
    public Equals(AbstractValidator next, String item, String pattern) {

        super(next, item);
        this.pattern = pattern;
        caseIgnore = false;
    }

    /**
     * Create a validator that test the given string against a pattern.
     * @param item to test
     * @param pattern to test against
     * @param caseIgnore if set to true, the case won't matter
     */
    public Equals(String item, String pattern, boolean caseIgnore) {

        super(item);
        this.pattern = pattern;
        this.caseIgnore = caseIgnore;
    }

    /**
     * Create a validator that test the given string against a pattern.
     * @param next validator to be executed
     * @param item to test
     * @param pattern to test against
     * @param caseIgnore if set to true, the case won't matter
     */
    public Equals(AbstractValidator next, String item, String pattern, boolean caseIgnore) {

        super(next, item);
        this.pattern = pattern;
        this.caseIgnore = caseIgnore;
    }

    @Override
    public boolean meetCriteria() {

        return caseIgnore ? item.equalsIgnoreCase(pattern) : item.equals(pattern);
    }

    @Override
    protected String getErrorMessage() {

        return "The string does not match.";
    }
}
