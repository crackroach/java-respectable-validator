package validation2.validators.string;

import validation2.AbstractValidator;

import java.util.regex.Pattern;

/**
 * Test if a String is composed of only digits
 */
public class Digit extends AbstractStringValidator{

    protected static final Pattern PATTERN = Pattern.compile("^[0-9]+$");

    public Digit(String item) {

        super(item);
    }

    public Digit(AbstractValidator next, String item) {

        super(next, item);
    }

    @Override
    public boolean meetCriteria() {

        return PATTERN.matcher(item).matches();
    }

    @Override
    protected String getErrorMessage() {

        return "Some character were not digit.";
    }
}
