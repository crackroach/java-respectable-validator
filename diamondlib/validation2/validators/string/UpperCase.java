package validation2.validators.string;

import validation2.AbstractValidator;

/**
 * Validate that all character are in uppercase
 */
public class UpperCase extends AbstractStringValidator {

    public UpperCase(String item) {

        super(item);
    }

    public UpperCase(AbstractValidator next, String item) {

        super(next, item);
    }

    /**
     * Test for all character if they are uppercase
     * Can be cpu consuming
     * @return if all character are upper case
     */
    @Override
    public boolean meetCriteria() {

        return item.toUpperCase().equals(item);
    }

    @Override
    protected String getErrorMessage() {

        return "One or more character were lowercase";
    }
}
