package validation2.validators.string;

import validation2.AbstractValidator;

/**
 * Base validator to evaluate String.
 */
public abstract class AbstractStringValidator extends AbstractValidator {

    /**
     * the string item to be evaluated
     */
    protected String item;

    /**
     * Define a constructor for AbstractStringValidator
     * @param item to be evaluated
     */
    public AbstractStringValidator(String item) {

        this.item = item;
    }

    /**
     * Define a constructor for AbstractStringValidator
     * @param next validator to be executed.
     * @param item to be evaluated.
     */
    public AbstractStringValidator(AbstractValidator next, String item) {

        super(next);
        this.item = item;
    }

    public String getItem() {

        return item;
    }
}
