package validation2.validators.string;

import validation2.AbstractValidator;

/**
 * Test a string for emptiness.
 */
public class Empty extends AbstractStringValidator {

    public Empty(String item) {

        super(item);
    }

    public Empty(AbstractValidator next, String item) {

        super(next, item);
    }

    @Override
    public boolean meetCriteria() {

        return item.isEmpty();
    }

    @Override
    protected String getErrorMessage() {

        return "The string is not empty.";
    }
}
