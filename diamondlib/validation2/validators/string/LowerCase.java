package validation2.validators.string;

import validation2.AbstractValidator;

/**
 * Test if all character are lower case
 */
public class LowerCase extends AbstractStringValidator {

    public LowerCase(String item) {

        super(item);
    }

    public LowerCase(AbstractValidator next, String item) {

        super(next, item);
    }

    /**
     * Test if all character are lower case
     * @return if all character are lower case
     */
    @Override
    public boolean meetCriteria() {

        return item.equals(item.toLowerCase());
    }

    @Override
    protected String getErrorMessage() {

        return "One or more character were in uppercase";
    }
}
