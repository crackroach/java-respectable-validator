package validation2.validators.string;

import validation2.AbstractValidator;

import java.util.regex.Pattern;

/**
 * Validate that a String contains only alphanumeric character
 */
public class Alnum extends AbstractStringValidator {


    /**
     * Regex pattern to test a string
     */
    protected static final Pattern PATTERN = Pattern.compile("[a-zA-Z0-9]+");

    public Alnum(String item) {

        super(item);
    }

    public Alnum(AbstractValidator next, String item) {

        super(next, item);
    }

    @Override
    public boolean meetCriteria() {

        return PATTERN.matcher(item).matches();
    }

    @Override
    protected String getErrorMessage() {

        return "The string contains one or more symbol";
    }
}
