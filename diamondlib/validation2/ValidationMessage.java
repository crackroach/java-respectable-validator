package validation2;

public class ValidationMessage {

    private String                                message;
    private String                                validator;
    private ValidationResultType type;

    public ValidationMessage(String message, String validator, ValidationResultType type) {

        this.message = message;
        this.validator = validator;
        this.type = type;
    }

    public ValidationMessage(String message, AbstractValidator validator, ValidationResultType type) {
        this(message, validator.getTag(), type);
    }

    public String getMessage() {

        return message;
    }

    public String getValidator() {

        return validator;
    }

    public ValidationResultType getType() {

        return type;
    }

    public boolean ok() {
        return type != ValidationResultType.ERROR;
    }
}
