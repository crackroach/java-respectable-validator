package validation2;

public enum ValidationResultType {
    VALID,
    WARNING,
    ERROR
}
