package validation2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ValidationResult {

    protected List<ValidationMessage> messages;

    public ValidationResult() {
        messages = new ArrayList<>();
    }

    public ValidationResult addValidationMessage(ValidationMessage message) {

        messages.add(message);
        return this;
    }

    public ValidationResult addValidationMessage(AbstractValidator validator, String message, ValidationResultType type) {

        return addValidationMessage(new ValidationMessage(message, validator, type));
    }

    public List<ValidationMessage> getMessages() {

        return messages;
    }

    public boolean isValid() {

        return getNumberOfError() == 0;
    }

    public int getNumberOfError() {
        int errors = 0;

        for(ValidationMessage vm : messages)
            errors += vm.ok() ? 0 : 1;

        return errors;
    }

    public int getNumberOfValid() {
        int valids = 0;

        for(ValidationMessage vm : messages)
            valids += vm.getType() == ValidationResultType.VALID ? 1 : 0;

        return valids;
    }

    public int getNumberOfWarning() {
        int warnings = 0;

        for(ValidationMessage vm : messages)
            warnings += vm.getType() == ValidationResultType.WARNING ? 1 : 0;

        return warnings;
    }

    public List<ValidationMessage> getMessagesByType(ValidationResultType type) {

        return messages.stream().filter(x -> x.getType() == type).collect(Collectors.toList());
    }
}
