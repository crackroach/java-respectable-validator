package validation2;


/**
 * Base validator for everything
 */
public abstract class AbstractValidator {

    /**
     * Next validator to be executed
     */
    protected AbstractValidator next;
    /**
     * Validation result of the composite validator
     */
    protected ValidationResult validationResult;
    /**
     * Name of the validation2
     */
    protected String name;
    /**
     * Used to specify which validator failed or validated correctly.
     * This is the validators name.
     */
    private String tag;

    /**
     * Create a basic AbstractValidator without any next subvalidator.
     */
    public AbstractValidator() {
        this(null);
    }

    /**
     * Create a basic AbstractValidator with a next subvalidator to be executed.
     * @param next next subvalidator to be executed.
     */
    public AbstractValidator(AbstractValidator next) {

        this.next = next;
        this.validationResult = new ValidationResult();
        tag = getClass().getSimpleName();
    }

    /**
     * Call validation2 on all subset of subvalidators.
     * @return that all of the validation2 passes the criteria.
     */
    public final ValidationResult validate() {

        if(!meetCriteria())
            validationResult.addValidationMessage(this, getErrorMessage(), ValidationResultType.ERROR);

        if(next != null)
            return next.validate();
        else {
            return validationResult;
        }
    }

    /**
     * This return the next validator to execute, but also passes the ValidationResult to the next validator.
     * @return the next subvalidator.
     */
    public final AbstractValidator next() {

        return this.next.setValidationResult(this.validationResult);
    }

    /**
     * Change the next validator in line.
     * @param next validator to be executed
     * @return this validator to chain commands
     */
    public AbstractValidator setNext(AbstractValidator next) {

        this.next = next;
        return this;
    }

    /**
     * Implementation depends on logic needed to acheive criteria
     * @return if the criteria is met.
     */
    public abstract boolean meetCriteria();

    /**
     * Sets the ValidationResult object to this validator
     * @param result is the validation2 result passed from a validator to another
     * @return this validator for easier chain command
     */
    private final AbstractValidator setValidationResult(ValidationResult result) {
        this.validationResult = result;
        return this;
    }

    /**
     *
     * @return a validator result to get errors and result
     */
    public ValidationResult getValidationResult() {

        return validationResult;
    }

    /**
     * The tag of this validator
     * @return
     */
    public String getTag() {

        return tag;
    }

    /**
     * A user friendly name for the validator. Good to use to differentiate between multiple validators
     * @return the name of this validator
     */
    public String getName() {

        return name;
    }

    /**
     * A user friendly name for the validator. Good to use to differentiate between multiple validators
     * @param name of the validator
     * @return this validator
     */
    public AbstractValidator setName(String name) {

        this.name = name;
        return this;
    }

    /**
     * Serves as an error message for when the validation2 fails.
     * @return a string containing why the test failed.
     */
    protected abstract String getErrorMessage();
}
