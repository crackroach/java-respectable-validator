import validation.AbstractValidator;
import validation.message.ValidationResult;
import validation.validators.numeric.Between;
import validation.validators.numeric.Even;
import validation.validators.numeric.Odd;
import validation.validators.string.Contains;
import validation.validators.string.Email;
import validation.validators.string.Phone;

public class Main {

    public static void main(String[] args) {

        AbstractValidator validator = new Email("Alexisdiamond@hotmail");

        //AbstractValidator validator = new Between(50, 10, 30, true);
        ValidationResult validationResult = validator.validate();

        System.out.println(String.format("Nb of errors: %s", validationResult.getMessages().size()));

        validationResult.getMessages().forEach(x -> {
            System.out.println(String.format("%s: %s", x.getShortValidator(), x.getMessage()));
        });

    }
}
