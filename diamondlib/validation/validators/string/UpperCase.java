package validation.validators.string;

/**
 * Validate that all character are in uppercase
 */
public class UpperCase extends AbstractStringValidator<UpperCase> {

    public UpperCase() {
        super();
    }

    public UpperCase(String item) {

        super(item);
    }

    /**
     * Test for all character if they are uppercase
     * Can be cpu consuming
     * @return if all character are upper case
     */
    @Override
    public boolean meetCriteria() {

        return item.toUpperCase().equals(item);
    }

    @Override
    protected String getErrorMessage() {

        return "One or more character were lowercase";
    }
}
