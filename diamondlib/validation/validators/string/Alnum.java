package validation.validators.string;

import java.util.regex.Pattern;

public class Alnum extends AbstractStringValidator<Alnum> {

    protected static final Pattern PATTERN = Pattern.compile("[a-zA-Z0-9]*");

    public Alnum() {

        super();
    }

    public Alnum(String item) {

        super(item);
    }

    @Override
    protected boolean meetCriteria() {

        return PATTERN.matcher(item).matches();
    }

    @Override
    protected String getErrorMessage() {

        return "The given string contains character other than " + PATTERN.toString();
    }
}
