package validation.validators.string;

public class Length extends AbstractStringValidator<Length> {

    protected int min;
    protected int max;

    public Length(int min, int max) {

        super();
        this.min = min;
        this.max = max;
    }

    public Length(String item, int min, int max) {

        super(item);
        this.min = min;
        this.max = max;
    }

    public int getMin() {

        return min;
    }

    public void setMin(int min) {

        this.min = min;
    }

    public int getMax() {

        return max;
    }

    public void setMax(int max) {

        this.max = max;
    }

    /**
     * Test if the string length is between a given minimum and a given maximum
     * @return if the string matches the required length
     */
    @Override
    public boolean meetCriteria() {

        final boolean value;

        if(min == -1) {
            value = item.length() < max;
        }
        else if(max == -1) {
            value = item.length() > min;
        }
        else {
            value = ((item.length() > min) && (item.length() < max));
        }

        return value;
    }

    @Override
    protected String getErrorMessage() {

        final String message;

        if(min == -1)
            message = String.format("The given string have a bigger length than %s.", max);
        else if(max == -1)
            message = String.format("The given string have a smaller length than %s.", min);
        else
            message = String.format("The given string does not have a length between %s and %s.", min, max);

        return message;
    }
}
