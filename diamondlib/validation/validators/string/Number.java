package validation.validators.string;

import java.util.regex.Pattern;
/**
 * Test a string to be a math.
 * Number can be decimal and/or negative as well.
 * <table>
 *     <caption></caption>
 *     <tr>
 *         <th>Number</th>
 *         <th>is valid?</th>
 *     </tr>
 *     <tr>
 *         <td>-09</td>
 *         <td style="background: green;">yes</td>
 *     </tr>
 *     <tr>
 *         <td>.2</td>
 *         <td style="background: red;">no</td>
 *     </tr>
 *     <tr>
 *         <td>0.45</td>
 *         <td style="background: green;">yes</td>
 *     </tr>
 *     <tr>
 *         <td>1,53</td>
 *         <td style="background: green;">yes</td>
 *     </tr>
 *     <tr>
 *         <td>1-5</td>
 *         <td style="background: red;">no</td>
 *     </tr>
 * </table>
 */
public class Number extends AbstractStringValidator<Number> {

    /**
     * A regex that validate if a string is only composed of numbers
     * Number can be decimal or negative.
     */
    protected final static Pattern PATTERN = Pattern.compile("^-?\\d+(\\.|,)?\\d*$");

    public Number() {
        super();
    }

    public Number(String item) {

        super(item);
    }

    @Override
    public boolean meetCriteria() {

        return PATTERN.matcher(item).matches();
    }

    @Override
    protected String getErrorMessage() {

        return "The given string is not a math";
    }
}
