package validation.validators.string;

public abstract class AbstractStringPatternValidator<T> extends AbstractStringValidator<AbstractStringPatternValidator> {

    protected String pattern;

    public AbstractStringPatternValidator(String pattern) {

        super();
        this.pattern = pattern;
    }

    public AbstractStringPatternValidator(String item, String pattern) {

        super(item);
        this.pattern = pattern;
    }

    @Override
    public AbstractStringValidator setNext(AbstractStringValidator next) {

        next.setItem(this.item);
        return super.setNext(next);
    }

    @Override
    public AbstractStringValidator setGetNext(AbstractStringValidator next) {

        next.item = this.item;
        return super.setGetNext(next);
    }
}
