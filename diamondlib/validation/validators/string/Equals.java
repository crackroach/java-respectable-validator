package validation.validators.string;

public class Equals extends AbstractStringComparatorValidator<Equals> {

    protected boolean caseIgnore;

    public Equals(String pattern, boolean caseIgnore) {

        super(pattern);
        this.caseIgnore = caseIgnore;
    }

    public Equals(String item, String pattern, boolean caseIgnore) {

        super(item, pattern);
        this.caseIgnore = caseIgnore;
    }

    @Override
    public boolean meetCriteria() {

        return caseIgnore ? item.equalsIgnoreCase(pattern) : item.equals(pattern);
    }

    @Override
    protected String getErrorMessage() {

        return "The string does not match.";
    }
}
