package validation.validators.string;

import java.util.regex.Pattern;

/**
 * Check if a string matches a given pattern
 */
public class Regex extends AbstractStringValidator<Regex> {

    /**
     * The pattern to check
     */
    protected Pattern pattern;
    protected boolean fullMatch;

    public Regex(String pattern, boolean fullMatch) {

        this("", pattern, fullMatch);
    }

    public Regex(String item, String pattern, boolean fullMatch) {

        super(item);
        this.pattern = Pattern.compile(pattern);
        this.fullMatch = fullMatch;
    }

    @Override
    public boolean meetCriteria() {

        return pattern.matcher(item).matches();
    }

    @Override
    protected String getErrorMessage() {

        return String.format("The String doesn't match the regex pattern.");
    }
}
