package validation.validators.string;

public class Contains extends AbstractStringComparatorValidator<Contains> {


    public Contains(String pattern) {

        super(pattern);
    }

    public Contains(String item, String pattern) {

        super(item, pattern);
    }

    @Override
    protected boolean meetCriteria() {

        return item.contains(pattern);
    }

    @Override
    protected String getErrorMessage() {

        return "The string item does not contain the requested pattern";
    }
}
