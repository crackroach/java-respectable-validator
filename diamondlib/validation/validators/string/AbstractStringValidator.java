package validation.validators.string;

import validation.AbstractValidator;

public abstract class AbstractStringValidator<T> extends AbstractValidator<AbstractStringValidator> {

    protected String item;

    public AbstractStringValidator() {
        super();
    }

    public AbstractStringValidator(String item) {
        super();
        this.item = item;
    }

    @Override
    public AbstractStringValidator setNext(AbstractStringValidator next) {

        next.item = this.item;
        this.next = next;
        return this;
    }

    @Override
    public AbstractStringValidator setGetNext(AbstractStringValidator next) {

        next.item = this.item;
        this.next = next;
        return next;
    }

    public AbstractStringValidator setItem(String item) {

        this.item = item;
        return this;
    }

    public String getItem() {

        return item;
    }
}
