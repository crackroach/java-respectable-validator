package validation.validators.string;

public abstract class AbstractStringComparatorValidator<T> extends AbstractStringValidator<AbstractStringComparatorValidator> {

    protected String pattern;

    public AbstractStringComparatorValidator(String pattern) {

        super();
        this.pattern = pattern;
    }

    public AbstractStringComparatorValidator(String item, String pattern) {

        super(item);
        this.pattern = pattern;
    }
}
