package validation.validators.string;

import java.util.regex.Pattern;

public class Alpha extends AbstractStringValidator<Alpha> {

    protected static final Pattern PATTERN = Pattern.compile("[a-zA-Z]*");

    public Alpha() {
        super();
    }

    public Alpha(String item) {

        super(item);
    }

    @Override
    protected boolean meetCriteria() {

        return PATTERN.matcher(item).matches();
    }

    @Override
    protected String getErrorMessage() {

        return "The given string contains non alphabetic characters";
    }
}
