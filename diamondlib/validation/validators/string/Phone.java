package validation.validators.string;

import java.util.regex.Pattern;

/**
 * Test if a string is a valid phone math
 * Support multiple phone math format
 * <ul>
 *     <li>xxx-xxx-xxxx</li>
 *     <li>xxxxxxxxxx</li>
 *     <li>xxxxxxxxxxx</li>
 *     <li>(xxx) xxx-xxxx</li>
 *     <li>x(xxx)xxxxxxx</li>
 *     <li>x(xxx)xxx-xxxx</li>
 *     <li>+x xxx xxx xxxx</li>
 *     <li>And many more</li>
 * </ul>
 */
public class Phone extends AbstractStringValidator<Phone> {

    private static final Pattern PATTERN = Pattern.compile("^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$");

    public Phone() {

        super();
    }

    public Phone(String item) {

        super(item);
    }

    @Override
    protected boolean meetCriteria() {

        return PATTERN.matcher(item).matches();
    }

    @Override
    protected String getErrorMessage() {

        return "The phone number provided is not in a valid format";
    }
}
