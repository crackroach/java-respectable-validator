package validation.validators.string;

public class LowerCase extends AbstractStringValidator<LowerCase> {

    public LowerCase() {
        super();
    }

    public LowerCase(String item) {

        super(item);
    }

    /**
     * Test if all character are lower case
     * @return if all character are lower case
     */
    @Override
    public boolean meetCriteria() {

        return item.equals(item.toLowerCase());
    }

    @Override
    protected String getErrorMessage() {

        return "One or more character were in uppercase";
    }
}
