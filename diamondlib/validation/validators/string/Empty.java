package validation.validators.string;

public class Empty extends AbstractStringValidator<Empty> {

    public Empty() {
        super();
    }

    public Empty(String item) {

        super(item);
    }

    @Override
    public boolean meetCriteria() {

        return item.isEmpty();
    }

    @Override
    protected String getErrorMessage() {

        return "The string is not empty.";
    }
}
