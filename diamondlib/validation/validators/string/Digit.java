package validation.validators.string;

import java.util.regex.Pattern;

public class Digit extends AbstractStringValidator<Digit> {

    protected static final Pattern PATTERN = Pattern.compile("^[0-9]+$");

    public Digit() {
        super();
    }

    public Digit(String item) {

        super(item);
    }

    @Override
    public boolean meetCriteria() {

        return PATTERN.matcher(item).matches();
    }

    @Override
    protected String getErrorMessage() {

        return "Some character were not digit.";
    }
}
