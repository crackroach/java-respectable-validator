package validation.validators.numeric;

public class Even extends AbstractNumericValidator<Even> {

    public Even() {
        super();
    }

    public Even(Number number) {

        super(number);
    }

    @Override
    protected boolean meetCriteria() {

        return number % 2 == 0;
    }

    @Override
    protected String getErrorMessage() {

        return "The number was odd.";
    }
}
