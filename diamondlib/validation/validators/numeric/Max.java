package validation.validators.numeric;

/**
 * Test that a number is smaller than the comparator
 */
public class Max extends AbstractNumericComparatorValidator<Max> {

    /**
     * Is the number is evaluated strictly. i.e. <
     * Defaulted to true (as in strictly smaller)
     */
    protected boolean strict;

    public Max(Number comparator) {

        this(comparator, true);
    }

    public Max(Number number, Number comparator) {

        this(number, comparator, true);
    }

    public Max(Number comparator, boolean strict) {

        super(comparator);
        this.strict = strict;
    }

    public Max(Number number, Number comparator, boolean strict) {

        super(number, comparator);
        this.strict = strict;
    }

    public boolean isStrict() {

        return strict;
    }

    public void setStrict(boolean strict) {

        this.strict = strict;
    }

    @Override
    protected boolean meetCriteria() {

        return strict ? number < comparator : number <= comparator;
    }

    @Override
    protected String getErrorMessage() {

        return strict ? "The number is bigger or equals than the comparator." : "The number is bigger than the comparator.";
    }
}
