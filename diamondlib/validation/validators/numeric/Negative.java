package validation.validators.numeric;

public class Negative extends AbstractNumericValidator<Negative> {

    public Negative() {
        super();
    }

    public Negative(Number number) {

        super(number);
    }

    @Override
    protected boolean meetCriteria() {

        return number < 0;
    }

    @Override
    protected String getErrorMessage() {

        return "The number was 0 or higher.";
    }
}
