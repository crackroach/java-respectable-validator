package validation.validators.numeric;

public abstract class AbstractNumericComparatorValidator<T> extends AbstractNumericValidator<AbstractNumericValidator> {

    protected Double comparator;

    public AbstractNumericComparatorValidator(Number comparator) {

        super();
        this.comparator = new Double(comparator.doubleValue());
    }

    public AbstractNumericComparatorValidator(Number number, Number comparator) {

        super(number);
        this.comparator = new Double(comparator.doubleValue());
    }

    public Number getComparator() {

        return comparator;
    }

    public void setComparator(Double comparator) {

        this.comparator = comparator;
    }
}
