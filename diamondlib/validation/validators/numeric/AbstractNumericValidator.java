package validation.validators.numeric;

import validation.AbstractValidator;

public abstract class AbstractNumericValidator<T> extends AbstractValidator<AbstractNumericValidator> {

    protected Double number;

    public AbstractNumericValidator() {
        this.number = new Double(0);
    }

    public AbstractNumericValidator(Number number) {

        this.number = new Double(number.doubleValue());
    }

    public Number getNumber() {

        return number;
    }

    public void setNumber(Double number) {

        this.number = number;
    }

    @Override
    public AbstractNumericValidator setNext(AbstractNumericValidator next) {

        next.number = this.number;
        return this;
    }

    @Override
    public AbstractNumericValidator setGetNext(AbstractNumericValidator next) {

        next.number = this.number;
        return next;
    }
}
