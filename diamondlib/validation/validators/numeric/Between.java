package validation.validators.numeric;

import validation.AbstractValidator;

/**
 * Test that a number is between a minimum and a maximum
 */
public class Between extends AbstractNumericValidator<Between> {

    protected Double min;
    protected Double max;
    /**
     * Are the limit included?
     * Defaulted at false
     */
    protected boolean inclusive;

    public Between(Number min, Number max, boolean inclusive) {

        super();
        this.min = new Double(min.doubleValue());
        this.max = new Double((max.doubleValue()));
        this.inclusive = false;
    }

    public Between(Number number, Number min, Number max, boolean inclusive) {

        super(number);
        this.min = new Double(min.doubleValue());
        this.max = new Double((max.doubleValue()));
        this.inclusive = inclusive;
    }

    @Override
    protected boolean meetCriteria() {

        return inclusive ? (number >= min && number <= max) : (number > min && number < max);
    }

    @Override
    protected String getErrorMessage() {

        String sentence = "The number was not between %s and %s";
        return inclusive ? String.format(sentence + " inclusively", min, max) : String.format(sentence, min, max);
    }
}
