package validation.validators.numeric;

public class Odd extends AbstractNumericValidator<Odd> {

    public Odd() {

        super();
    }

    public Odd(Number number) {

        super(number);
    }

    @Override
    protected boolean meetCriteria() {

        return number % 2 != 0;
    }

    @Override
    protected String getErrorMessage() {

        return "The number was even";
    }
}
