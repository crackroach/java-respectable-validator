package validation.validators.numeric;

public class Equals extends AbstractNumericComparatorValidator<Equals> {

    public Equals(Number comparator) {

        super(comparator);
    }

    public Equals(Number number, Number comparator) {

        super(number, comparator);
    }

    @Override
    protected boolean meetCriteria() {

        return number == comparator;
    }

    @Override
    protected String getErrorMessage() {

        return "The number was different from the comparator.";
    }
}
