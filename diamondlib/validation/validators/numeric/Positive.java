package validation.validators.numeric;

public class Positive extends AbstractNumericValidator<Positive> {

    public Positive() {
        super();
    }

    public Positive(Number number) {

        super(number);
    }

    @Override
    protected boolean meetCriteria() {

        return number > 0;
    }

    @Override
    protected String getErrorMessage() {

        return "The number was 0 or lower";
    }
}
