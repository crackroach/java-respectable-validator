package validation.validators.numeric;

/**
 * Test that a number is bigger than the comparator
 */
public class Min extends AbstractNumericComparatorValidator<Min> {

    /**
     * Is the number is evaluated strictly. i.e. >
     * Defaulted to true (as in strictly bigger)
     */
    protected boolean strict;

    public Min(Number comparator) {

        this(comparator, true);
    }

    public Min(Number number, Number comparator) {

        this(number, comparator, true);
    }

    public Min(Number comparator, boolean strict) {

        super(comparator);
        this.strict = strict;
    }

    public Min(Number number, Number comparator, boolean strict) {

        super(number, comparator);
        this.strict = strict;
    }

    public boolean isStrict() {

        return strict;
    }

    public void setStrict(boolean strict) {

        this.strict = strict;
    }

    @Override
    protected boolean meetCriteria() {

        return strict ? number > comparator : number >= comparator;
    }

    @Override
    protected String getErrorMessage() {

        return strict ? "The number is smaller or equals than the comparator." : "The number is smaller than the comparator.";
    }
}
