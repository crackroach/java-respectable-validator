package validation.cast;

import validation.validators.numeric.AbstractNumericValidator;

public interface ICastToNumber {
    AbstractNumericValidator asNumber();
}
