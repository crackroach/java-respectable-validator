package validation.cast;

import validation.validators.string.AbstractStringValidator;

public interface ICastToString {
    AbstractStringValidator asString();
}
