package validation;

import validation.message.ValidationResult;

public abstract class AbstractValidator<T> {

    public static final String VALIDATOR_ALREADY_EXECUTED = "This validator has already been executed.";

    protected AbstractValidator next;
    protected String            tag;
    protected ValidationResult  validationResult;
    protected boolean executed;


    protected AbstractValidator() {
        this.tag = getClass().getName();
        validationResult = new ValidationResult();
        executed = false;
    }

    public final AbstractValidator getNext() {
        return next;
    }

    public abstract T setNext(T next);
    public abstract T setGetNext(T next);

    public AbstractValidator setValidationResult(ValidationResult validationResult) {

        this.validationResult = validationResult;
        return this;
    }

    protected abstract boolean meetCriteria();

    public String getTag() {

        return tag;
    }

    public ValidationResult getValidationResult() {

        return validationResult;
    }

    public final ValidationResult validate() {

        if(!executed) {
            boolean criteriaMet = meetCriteria();
            executed = true;

            if (next != null) {
                validationResult = setValidationResult(validationResult).next.validate();
            }

            if(!criteriaMet) {
                validationResult.addMessage(tag, getErrorMessage());
            }
        }
        else
            validationResult.addMessage(tag, VALIDATOR_ALREADY_EXECUTED);

        return validationResult;
    }

    /**
     *
     * @return if the Validator has been executed
     */
    public boolean isExecuted() {

        return executed;
    }

    protected abstract String getErrorMessage();
}
