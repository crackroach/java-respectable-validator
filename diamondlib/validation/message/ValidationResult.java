package validation.message;

import java.util.ArrayList;
import java.util.List;

public class ValidationResult {

    protected List<ValidationMessage> messages;

    public ValidationResult() {

        this.messages = new ArrayList<>();
    }

    public void addMessage(String validator, String message) {
        messages.add(new ValidationMessage(validator, message));
    }

    public boolean isValid() {
        return messages.size() == 0;
    }

    public List<ValidationMessage> getMessages() {

        return messages;
    }
}
