package validation.message;

public class ValidationMessage {

    protected String validator;
    protected String message;

    public ValidationMessage(String validator, String message) {

        this.validator = validator;
        this.message = message;
    }

    public String getValidator() {

        return validator;
    }

    public String getShortValidator() {

        int lastIndex = validator.lastIndexOf(".");
        return validator.substring(lastIndex + 1, validator.length());
    }

    public String getMessage() {

        return message;
    }
}
